<?php

namespace App\Controller;

use App\Entity\Tienda;
use App\Repository\TiendaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

class ListController extends AbstractController
{
    /**
     * @Route("/list", name="app_list")
     */
    public function index(TiendaRepository $tiendaRepository): Response
    {
        return $this->render('list/index.html.twig', [
            'articulos' => $tiendaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="app_delete")
     */
    public function delete(Tienda $tienda, ManagerRegistry $doctrine): RedirectResponse
    {
        $em = $doctrine->getManager();
        $em->remove($tienda);
        $em->flush();

        return $this->redirectToRoute('app_list');
    }
}
